#!/usr/bin/py
import os
import sys
import requests
from HTMLParser import HTMLParser
import time
from urlparse import urlparse


class MyHtmlParser(HTMLParser):
	'''Parses the html and extracts all links'''

	def __init__(self):
		HTMLParser.__init__(self)
		self.link_count = 0
		self.pages_to_visit = []

	def handle_starttag(self, tag, attrs):
		'''Find all links'''
		if tag == 'a':
			for attr in attrs:
				if attr[0] == 'href':
					#print attr[1]
					self.pages_to_visit.append(attr[1])
					self.link_count += 1

	def handle_endtag(self, tag):
		pass
#		print "Endtag", tag

	def handle_data(self, data):
		pass
#		print "Some data", data


class Crawler(object):

	def __init__(self, root):
		self.visited = []
		self.to_visit = [root]
		self.htmlparser = MyHtmlParser()

	def crawl(self):
		while len(self.to_visit) > 0:
			url = self.to_visit.pop(0)

			if not self.is_url(url) or url in self.visited or not self.is_same_domain():
				print "Skipping %s" % (url)
				continue
			
			self.visited.append(url)
			print self.visited
			print "Crawling %s" % (url)

			r = requests.get(url)
			self.htmlparser.feed(r.text)
			
			for link in self.htmlparser.pages_to_visit:
				self.to_visit.append(link)
				
			print 'Links added: ', self.htmlparser.link_count
			#time.sleep(0.1)
			raw_input("Press enter to continue..")


	def is_same_domain(self, s, domain):
		url = urlparse(s)
		
		if url.netloc == domain:
			return True

		return False

	def is_url(self, s):
		url = urlparse(s)
		
		if url.scheme == 'http':
			return True
		return False



if __name__ == '__main__':
	crawler = Crawler('http://www.vg.no')
	crawler.crawl()
