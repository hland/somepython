#!/usr/bin/python
'''
Script for finding greatest common divisors using python
'''

import sys


def gcd(a, b):
    '''Calculates the greatest common divisor between two integers a and b'''
    a = int(a)
    b = int(b)
    original_a = a
    original_b = b

    if a < b:
        tmp = a
        a = b
        b = tmp

    while True:
        remainder = a % b
        a = b

        if remainder == 0:
            print "GCD of %s and %s is %i" % (original_a, original_b, b)
            if b == 1:
                print "The numbers are also coprime"
            return b    

        b = remainder 


if __name__ == "__main__":
    if len(sys.argv) == 3:
        gcd(sys.argv[1], sys.argv[2])
    
    else:
        print "Usage %s number1 number2" % (sys.argv[0])    
