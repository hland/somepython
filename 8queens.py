import numpy
import copy
import time
from random import *
import math
import sys


class EightQueens:

	def __init__(self):
		self.current = numpy.arange(64)
		self.current.shape = (math.sqrt(self.current.size), math.sqrt(self.current.size))
		self.current.fill(0)
		self.__placeQueensRandomly()
		self.numberOfConflicts = 0

		self.dim = self.current.shape[0]


	def solve(self, maxTries = 1000):
		'''Tries to solve the puzzle'''
		
		tries = 0

		while not self.__isGoal() and tries < maxTries:
			x = randint(0, self.dim -1)
			self.__improveColumn(x)

			tries += 1

		if tries == maxTries:
			print "A solution was not found within the time limit"
			return False

		else:
			print "A solution was found:\n{0}".format(self.current)
			print "Number of iterations needed: {0}".format(tries)
			return True


	
	def __improveColumn(self, column):
		'''Moves the queen to a new position that yields the fewest conflicts'''

		move = self.__checkColumn(column)

		self.current[move[0]] = 0
		self.current[move[1]] = 1



	def __isGoal(self):

		queens = self.current.nonzero()
		queens = zip(queens[0], queens[1])

		for queen in queens:
			#print "Queen is at: {0}:".format(queen)
			#print "Number of conflicts: {0}".format(self.__checkElement(queen[0], queen[1]))
			if self.__checkElement(queen[0], queen[1]) > 0:
				return False
			
		return True


	
	def __placeQueensRandomly(self):
		dimension = self.current.shape[0]

		for x in range(dimension):	
			p = (randint(0, dimension-1), x)
			self.current[p] = 1


	def __checkColumn(self, column):
		'''Checks the given column-index to find
		the move that yields the least number of conflicts.
		Returns two tuples: The queens current position and the new best position.'''

		dim = self.current.shape[0]
		conflicts = [0 for x in range(dim)]

		currentQueenRow = numpy.nonzero(self.current[:,column])
		currentQueenRow = currentQueenRow[0][0]

		rowsToCheck = [x for x in range(dim) if x != currentQueenRow]

		for row in rowsToCheck:
			conflicts[row] = self.__checkElement(row, column)- 1 # Don't count the queen at the current position

		conflicts[currentQueenRow] = self.__checkElement(currentQueenRow, column)
		#print conflicts

		bestNewPosition = (conflicts.index(min(conflicts)), column)

		return ((currentQueenRow, column), bestNewPosition)

	
	def __checkElement(self, row, column):
		'''Finds the number of conflicts that occur in a given cell'''

		dim = self.current.shape[0]
		aFlipped = numpy.fliplr(self.current)

		#print "({0}, {1})".format(row, column)

		d1 = numpy.diagonal(self.current, column - row)
		d1 = numpy.count_nonzero(d1)
		#print "Conflicts on diagonal(ul, br): {0}".format(d1)

		d2 = numpy.count_nonzero(numpy.diagonal(aFlipped, (dim - column - 1) - row))
		#print "Conflicts on diagonal(bl, tr): {0}".format(d2)

		horizontal_conlicts = numpy.count_nonzero(self.current[row])
		#print "Conflicts on row: {0}".format(row)

		vertical_conflicts = numpy.count_nonzero(self.current[:, column])
		#print "Conflicts on column: {0}".format(col)

		if self.current.item((row, column)) == 1:
			return d1 + d2 + horizontal_conlicts + vertical_conflicts - 4	
			

		return d1 + d2 + horizontal_conlicts + vertical_conflicts	


		

		
			

result = [0, 0]


for x in range(100):
	a = EightQueens()
	if a.solve():
		result[0] += 1
	else:
		result[1] += 1

print "Solved: {0}\nFailed: {1}".format(result[0], result[1])





