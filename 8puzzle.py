import numpy
import copy
import time


class EightPuzzleState:

	def __init__(self, s, cost = 0, parent = None):
		#print "Dimensions: {0}".format(s.shape)
		#print "Starting state:\n{0}\n".format(s)
		self.state = s
		self.cost = cost
		self.manhattanDistance = self.__manhattanDistance()
		self.parent = parent

		if self.state.shape[0] != self.state.shape[1]:
			raise ValueError("This puzzle requires a square matrix")

	def evaluate(self):
		return self.cost + self.manhattanDistance

	def cost():
		return self.costSoFar

	def __manhattanDistance(self):
		'''Returns the Manhattan value of the puzzle'''

		m = numpy.array(self.state)
		manhattanSum = 0
		dimension = self.state.shape[0]

		puzzleValues = [i for i in range(0, self.state.size -1)]

		for x in puzzleValues:
			p0 = (x / dimension, x % dimension)
			p1 = numpy.where(m == x+1)
			p1 = (p1[0][0], p1[1][0])
			manhattanSum = manhattanSum + (abs(p1[0] - p0[0]) + abs(p1[1] - p0[1]))

		p0 = (dimension -1, dimension -1)
		p1 = numpy.where(m == 0)
		p1 = (p1[0][0], p1[1][0])
		manhattanSum = manhattanSum + (abs(p1[0] - p0[0]) + abs(p1[1] - p0[0]))

		return manhattanSum

	def __eq__(self, other):
		return numpy.array_equal(self.state, other.state)

	def __lt__(self, other):
		return self.manhattanDistance < other.manhattanDistance

	def __str__(self):
		return "{0}".format(self.state)

	def __repr__(self):
		return "{0}".format(self.state)


	def print_state(self):
		'''Prints a representation of the 8-puzzle'''
		for x in self.state:
			print x

	def isGoal(self):
		'''Checks if the goal state has been reached'''

		#time.sleep(5)

		#print "Starting state:\n{0}\n".format(self.state)
		
		c = 0;
		for i, x in enumerate(self.state.flat):
			if x > c or i == self.state.size-1:
				c = x
			else:
				return False

		return True

	def __locateSpace(self):
		'''Returns the coordinates of the empty square'''

		m =  numpy.array(self.state) # Possible error source. Might need a deepcopy here

		for i,row in enumerate(m):
			for j,col in enumerate(row):
				if col == 0:
					return (i, j)

	def makeMove(self, matrix, e1, e2):
		'''Swaps two elements in a matrix and returns the new 
		matrix'''

		newMatrix = copy.deepcopy(matrix)
		tmp = newMatrix.item(e1)
		newMatrix[e1] = newMatrix.item(e2)
		newMatrix[e2] = tmp

		return newMatrix


	def expand(self):
		'''Generate new states from the current state'''
		successorStates = []
		emptyTilePosition = self.__locateSpace()
		permittedRange = range(self.state.shape[0])

		#print "Empty tile position: {0}\n".format(emptyTilePosition)

		moves = [(emptyTilePosition[0] - 1, emptyTilePosition[1]),
							(emptyTilePosition[0], emptyTilePosition[1] - 1),
							(emptyTilePosition[0], emptyTilePosition[1] + 1),
							(emptyTilePosition[0] + 1, emptyTilePosition[1])]

		for move in moves:
			if move[0] in permittedRange and move[1] in permittedRange:

				#print "Moves to make: {0}".format(move)
				successorStates.append(EightPuzzleState(self.makeMove(self.state, emptyTilePosition, move), self.cost + 1, self))

		return successorStates

		
	


def Search(startState):
	'''Searches for a goal state using A* search. This method should be able
	to handle any problem given to it as long as the problem
	instance has a starting state, knows how to generate successorstates
	and has some admissable heuristic'''


	startTime = time.time()
	queue = [startState]
	exploredStates = []
	lowestEvaluation = float('inf')
	numberOfStatesExpanded = 0

	while queue:
		queue.sort()
		currentState = queue.pop(0)

		if time.time() > startTime + 10:
			startTime = time.time()
			print "Number of states explored: {0}".format(len(exploredStates))

		if not currentState.isGoal():
			exploredStates.append(currentState)
			for successorState in currentState.expand():
				numberOfStatesExpanded += 1
				if successorState not in exploredStates:
					queue.append(successorState)
		
		else:
			
			solution = traceSteps([currentState])

			for step in solution:
				print "{0}\n".format(step)

			
			print "Hooray, we found a solution! :)"
			print "The solution is {0} steps long".format(len(solution))
			print "The search expanded {0} nodes to find the solution".format(numberOfStatesExpanded)
			return True


def traceSteps(state):

	if state[0].parent is None:
		return state
		
	return traceSteps([state[0].parent] + state)
	
	

				


s = numpy.matrix('1 5 2; 4 6 0; 3 8 7')
t = numpy.matrix('7 2 4; 5 0 6; 8 3 1')
u = numpy.matrix('7 2 4 9 10; 5 0 6 11 12; 8 3 1 13 14; 15 19 18 17 16; 24 21 22 23 20')
v = numpy.matrix('7 2 4 9; 5 0 6 10; 8 3 1 11; 12 13 14 15')

s2 = numpy.matrix('1 2 3; 4 5 6; 8 0 7')
g = numpy.matrix('1 2 3; 4 5 6; 7 8 0')

startState = EightPuzzleState(v)
Search(startState)
'''Curently just works for 3x3 matrices'''


